FROM node:18-alpine AS base

FROM base AS installer
WORKDIR /app

COPY package.json ./

# First install the dependencies (as they change less often)
RUN npm i

COPY . .

# Build the project
RUN npm run build 

# Prepare for deployment
FROM base AS runner
WORKDIR /app

# Copy necessery file for SvelteKit Node Adapter
COPY --from=installer /app/build ./build
COPY --from=installer /app/package.json ./package.json

# Install dependencies (make sure shared packages are in devDependencies so it won't be installed here)
RUN npm i --omit dev

# Run the program
EXPOSE 3000
CMD PORT=3000 node build